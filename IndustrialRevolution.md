# What opposed the Bourgeoisie and the proletariat?

During the Industrial Revolution, society was clearly divided into two classes: the bourgeoisie, and the proletariat, each with its own subclasses. The most obvious difference, as underlined by Document 1, is wealth. The bourgeois become really rich on the backs of the proletariat, and are symbolised here by the elegantly dressed man and two ladies in grotesquely huge dresses. They are also clearly drunk. On the side of the image, we can see members of the proletariat, dressed in simple, dirty clothes, clearly being poor and disdainful towards the bourgeois.

In Document 2, an excerpt of Karl Marx's *Manifesto of the communist party*, the wealth gap and exploitation of the proletariat is clearly depicted: Proletarians are reducted to little more than objects, used only to "perform only the simplest, most monotonous and quickest learned operation". They lose all autonomy and are mostly at the mercy of the ones controlling the factory they work in.

Document 3 indicates what a bourgeois is, and starts off by stating that what makes someone a bourgeois is, before money, appearance. Bourgeois led a prestigious life, the goal of which was to be rich, but also appear as rich as possible. They would have carefully furnished houses, always eat at a laid table, and the living room was more often than not, home to some visitors. Due to the low wages on the proletariat, even the most modest of bourgeois could afford at least one housemaid, with there often being a dedicated cook, valet, chambermaids, etc. all of this leading to a bourgeois being jokingly defined as "he who has a piano in his living room"

Finally, Document 4 shows a proletarian family sitting around a table dimly lit by a naphtha lamp, eating potatoes out of a single dish. This shows the sheer poverty of the proletarians, as well as their physical weakness and deformity as represented by their distorted and grubby faces.

