# Notes and answers to the imperialism worksheet

## Economic and social motivations

### *Present the document using DANDI (Rhodes)*

D:1895, after the author had attended a meeting about unemployment
A:Cecil Rhodes
N:A conversation with by W.T.Stead, and later published by Lenin (not entirely reliable)
D:His journalist friend (W.T.Stead)
I:Discuss solutions for homelessness and famine, especially colonization

### *Present the document using DANDI (Ferry)*

D:March 28, 1884
A:Jules Ferry
N:Speech to the french parliament
D:The French Chamber of Deputies
I:Encourage imperialism

### *What are the arguments made by Ferry and Rhodes?*

They believe imperialism and colonization to create new settlements, as well as a way to gain more trade outlets and goods.

### *What other motivations can you guess from the map?*

The French want to cultivate exotic plants and spices, as well as rubber trees in newly conquered territories with an adequate climate, probably to trade them to the rest of Europe and get richer.

## Political and ideological justification

### *According to the documents, what is the duty of Europeans towards the rest of the world? What can they bring to it?*

They want to educate and develop it, bringing peace and prosperity.

### *Why do Europeans believe they have a duty towards the rest of the world?*

They believe in a hierarchy of races, with them at the top. That ideology is called racialism.

### *Present the above document.*

D:1877
A:Unknown
N:Extract from a school textbook
D:Schoolchildren
I:Educate children about the racial beliefs of the late XIX century

## Forms of colonialism and imperialism

Colony: The metropole/parent state is fully in charge of the colony. Ex. Algeria, South Africa, Hong Kong

Protectorate: The parent state leaves part of the administration of the country intact, but is still in control. Ex. Syria, Cambodia, Vietnam 

Sphere of influence: The country affected keeps its independencn, but the imperialist country gets rights to part of the territory. Ex. Liberia and US

### *Explain the advantages and inconvenients of each form of imperialism.*

Colony: Foreign country is in control - less risk of rebellion / very costly to send people to rule it

Protectorate: Cheaper - less people/army to control the country / Less control/higher risk of rebellion

Sphere of influence: Mainly used for trade, if the country is too big to control, to avoid rivalry / no control over government, less sustainable

## The effects of colonial rule

### *Did France carry out its civilizing mission in Vietnam?*

No, it did not, and did not bring education, health or security to Vietnam. The economy deteriorated and by 1939, 80% of the population was illiterate.

### *The French invested a lot of money into the colonies. Why did it not benefit the indigenous populations?*

All enterprises were in foreign hands, nothing important was run by the locals.

