# How did napoleon change the course of the French Revolution?

## Introduction

Napoleon is arguably one of the most iconic french generals and rulers. His first rise to power in 1799 as First Consul can be seen as an end to the Revolution and the comeback to a system that is somewhat similar to the one before the Revolution. A question poses itself though: As much as it is an end to the revolution, does it also mark an end to revolutionary ideas?

> 150 words, present topic, context, argument, structure of your answer

## []

>500-700 words, different sections/parts, 1 example/idea/argument in each, 2-4 parts

>present all documents, list revolutionary values and compare them to napoleon's

## Conclusion

>2 subparts, 150-200 words answer initial claim, open up with another question
