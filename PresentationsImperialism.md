# Notes on the presentations on imperialism

## Livingstone

|> Joined the London Missionary Society, an association wanting to evangelise the forld

|> Livingstone wanted to abolish the slave trade and find the source of the Nile

|> Explored Africa to free ppl from slavery, evangelise them and map out the continent

|> Gained a reputation from travels and was considered a national hero

|> Published books about his travels and slavery, sold out immediately

|> Livingstone disappears on an expedition to find the source of the Nile, is then found and dies two years later

|> Found the source of the Nile (Victoria Falls) but didnt know that it was the source

|> Precursor of colonization: Had good relations with natives, gave Europeans knowledge of African geograghy, inspired other explorers

## Boer wars

|> Portuguese discovered S.Africa, the Dutch colonized it, lost it to Napoleon, then England got it somehow

|> "Boer" -> Dutch person living in.S Africa. Felt marginalized and moved inwards to Africa, creating 3 new states

|> England wanted control of S.Africa: Military power, trade route to India

|> 1st boer war: boers fought for independence, won because of better organisation and positional advantage

|> Resulted in admission of transvaal independence

|> Gold was found in transvaal, the government closed down borders which caused protests from british people living there

|> Jameson Raid: raid into transvaal timed with a planned uprising there

|> 2nd boer war causes: Jameson raid, gold found in transvaal, political tensions, restrictions on voting

|> Boers attacked from 2 sides, but fought back and besieged british border cities

|> British reinforcements arrive, fight back, boer president escapes

|> Boers use guerilla warfare, their farms are destroyed and are held up in camps in horrible conditions

## Code de l'indigenat

|> Set of arbitrary laws discriminating against natives in colonies

|> In Algeria: Established in 1875, codified in 1881, formalized de facto discrimination of indigenes, was supposed to last 7 years

|-> 2 Types of people: French citizens and French subjects

|> Spread to other french colonies around the world

|> Made to maintain the colonial order, control the population and prevent uprisings

|> Arbitrary punition, unfair, no trials

|> Punishments went from fines to execution or corporal punishment

|> Charles Depince: Justifies the right to discriminate indigenous people bc the french won the war against them

|> Emile Larcher: Calls out the code as unjust, violent and against freedom

|> Ended in 1946, but progressively over the previous 20 years, with decrees lowering fines and punishments, as well as the Geneva convention

