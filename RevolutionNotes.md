# Random Notes on the French Revolution

## National Assembly (1789-1791)

    > Starts at the Estates General
    > Ends w/flight to Varennes

## Legislative Assembly (1791-1792)

    > Constitution passed
    > Limited monarchy
    > Elected deputees

## Convention (1792-1795)

    > King overthrown --> 1st republic in France
    > Same people as L. Assembly and N. Assembly --> people carry on from older governments
    > Louis XVI is trialed and beheaded

## Directory (1795-1799)

    >
