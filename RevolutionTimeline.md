# French Revolution Timeline

|| *6/6/1789* **Estates General Opened**

|| *20/6/1789* **Tennis Court Oath is sworn and National Assembly is formed**

|| *04/08/1789* **Abolition of privileges**

|| *25/8/1789*  **Declaration of Human Rights**

|| *12/7/1790* ***On the Admission of women to citizenship* is published**

|| *05/10/1789* **Women march on Versailles and force the royals to return to Paris**

|| *June 1791* **Arrest of the royals after attempt to flee**

|| *April 1792* **Legislative Assembly declares war on Austria**

|| *August 1792* **Louis XVI is arrested**

|| *September 1792* **Convention becomes the new political system (first Republic is created)**

|| *January 1793* **Louis XVI is decapitated**

\ /
