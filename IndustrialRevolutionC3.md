# Answers to the Chapter 3 worksheet

[Reference for questions, might be removed later](https://florimontch.sharepoint.com/sites/M-2M3-HIS-B-TJAC/_layouts/15/Doc.aspx?sourcedoc={070eebe1-7d0f-4cb6-a51c-92c358ca05e9}&action=view&wd=target%28_Biblioth%C3%A8que%20de%20contenu%2FChapter%203%20-%20The%20Industrial%20Revolution.one%7C03db8268-df22-434f-b208-dc6b12e00d7f%2FII.%20Social%20Issues%20and%20New%20Political%20Theories%7Cb39b2cf9-fd6e-2d4a-a474-4e8705609708%2F%29&wdorigin=703)

## The Sadler Report

### *Conduct a quick search to find out who Michael Thomas Sadler was. Find out about his background, political party and commitements*

He was a member of the British Parliament who advocated for laws which would limit child labour, mostly by the means of publishing the "*Sadler report*"

### *Continue your research to document Sadler's 1833 report. What was it? What were its main aspects?*

It analysed the physical and psychological effects of intense work on British children, to show the bad treatment and lack of food and sleep they had. It would later serve as the basis for the Factory Act of 1833.

### *Who was Samuel Smith?*

He was a surgeon in Leeds (England)

### *What did he describe?*

The poor physical condition of English children.

### *What did he think of a legislative enactment to improve working conditions in factories?*

He wanted the State to regulate them, as nothing else would have any effect.

## The Factory Act

### *Who gave evidence to the factory inspector?

The children who worked in the factory.

### *How many hours the boys were reported to have worked without major breaks*

About 30.

### *Which parts of the Act were broken?*

The work hour limits, nightly work and not going to school.

### *What was the most common offense?*

Employing children after a certain hour.

### *Which offense was the most serious?*

Employing young people without medical certificates.

### *Analise the photo*

It was taken way into the second industrial revolution, and about 70 years after the Act. It is somewhat propaganda as it shows a good work environment. 

### *What does it tell us about the evolution of child labour?*

It is not a very reliable source. The workers look too clean and happy, and can only lead us to conclude that teenagers still worked in factories in 1903.

## Laws to improve working conditions in factories

### *How did the legislation evolve? What conclusions can you draw?*

It evolved towards more limited work, starting from the textile industry and was later generalised. It was rather slow and gradually took ~60 years to implement.

### *What comparisons can be made between the two countries?*

In Switzerland, legislation is cantonal, instead of national.

## Industrial Paternalism

### *Summarise Krupp's advice*

Workers should stay at home and not get involved in politics. They should care about their family and their education. He also tells his employees not to drink and "politicise at the pub".

### *What does "paternalism" mean(or industrial paternalism)?*

It is capitalism that includes social wellfare. Businesses providing wellfare services to employees.

### *How is Krupp a good example of paternalism?*

Krupp built his workers houses and gave them advice on how to behave and care for their family.

### *Search the internet to find what social democracy is in the XIX century*

It was a labor movement within socialism which wanted to replace private ownership and replace it with public one. It was strongly influenced by Marxism.

### *Point out Krupp's view on social democracy*

He is strongly opposed to it, as he fears strikes and wants to keep his ownership of the factory and workers.

## Liberalism

### *What was the "Invisible Hand"?*

It is a metaphor used by Smith to describe the principles that move the free market. The best interest of society would be achieved by self-interest.

### *How do you interpret the underlined sentence?*

The market will always reach an equilibrium without outside intervention from the government or other intervention.

### *According to Smith, what should be the role of governments?*

Governments should keep the country at peace, be in charge of taxes, and keep them low, as well as administrate justice. He also encourages, although in different documents, the state
to pay for public education and keep a high literacy rate.

## Robert Owen's utopian socialism

### *Proceed to an internet search to document who Robert Owen was*

According to Wikipedia, Robert Owen was a Welsh textile manufacturer, philantropist and social reformer, and a founder of utopian socialism and the cooperative movement. He strove to
improve factory working conditions, promoted socialistic communities, and sought a more collective approach to child rearing.

### *Explain how he interpreted the society of his time*

He makes an analogy between their employees and machines, stating that a "well-maintained" worker is more efficient, advocating that caring for their workers will yield more gain in
the long term.

### *Proceed to an internet search to find who Karl Marx and Friedrich Engels were*

According to Wikipedia:
Karl Heinrich Marx was a German philosopher, economist, sociologist, political theorist, and socialist revolutionary. He wrote *The Communist Manifesto* and the three-volume *Das Kapital*.
Friedrich Engels was a German philosopher, political theorist and revolutionary socialist.

### *Present the above document*

Date: 1848, middle of the industrial revolution
Author: Marx and Engels, see last question
Nature: Manifesto (argumentative writing with a strong message)
Destination: People of that time, specifically the proletariat
Intention: Describe the authors' proposed political system

### *According to Marx and Engels, what are the two antagonist social classes in industrial societies?*

It is the bourgeois and the proletariat.

### *What differentiates the two?*

The bourgeois own the means of prioduction and get most of the profit from it, while the proletarians live only from their work, owning nearly nothing.

### *According to them, what is the role of the proletariat?*

### *What is the message of the caricature of capitalism?*

