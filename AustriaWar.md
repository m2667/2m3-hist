# The war with Austria and changes in the French government

## Causes of the war

    > April 1791: Legislative Assembly declares war on Austria
    > Both the king and the Asembly wanted the war but for different reasons
        > The king wants the Revolution to end through the victory of the Allied powers
        > The Legislative Assembly thinks the Revolution will be strengthened in fight of a common enemy

## The war

    > First months --> Series of defeats for the French
    > September 1792 --> Turn of events, France starts winning
    > Napoleon is made general in 1793

## Political Events

    > Louis XVI is arrested in August 1792, tried in December 1792, and beheaded in January 1793
    > The 1st Republic is formed in September 1792
    > The calendar is reformed in October 1793
